## Descarga

```bash
 git clone https://gitlab.com/cdtombe/react-rick.git
```

## Instalacion

```bash
  npm install 
```

## Ejecucion del aplicativo

```bash
  npm run dev 
```

## Ejecucion de las pruebas

```bash
  npm run test
```